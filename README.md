
step by step:
Clone the repository:
```bash
git clone https://gitlab.com/duong-anh/nextjs-interview-df.git
```
Navigate to the project directory:
```bash
cd nextjs-interview-df
```
Install the dependencies:
```bash
npm install
```
or
```bash
yarn install
```
Development
To start the development server, run:
```bash
npm run dev
```
or
```bash
yarn dev
```
This will start the Next.js development server and you can access the application in your browser at http://localhost:3000/signin.


Demo:
```bash
https://interview-nextjs-j49jh9cz7-duonghoanhs-projects.vercel.app/signin
```

Thời gian có hạn và do em sửa bậy nên hỏng mất một cái call api