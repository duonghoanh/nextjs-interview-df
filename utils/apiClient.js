import axios from 'axios';

const apiClient = axios.create({
  baseURL: 'https://frontend-exam.digitalfortress.dev',
  headers: {
    'Content-Type': 'application/json',
  },
});

// Add any necessary request interceptors, such as adding auth headers
apiClient.interceptors.request.use(
  (config) => {
    // Add access token to the request headers
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      config.headers.Authorization = `Bearer ${accessToken}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);


apiClient.interceptors.response.use(
  (response) => {
    return response;
  },
  async (error) => {
    const originalRequest = error.config;
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      try {
        const response = await apiClient.post('/auth/refresh', {
          accessToken: localStorage.getItem('accessToken'),
          refreshToken: localStorage.getItem('refreshToken'),
        });
        localStorage.setItem('accessToken', response.data.accessToken);
        return apiClient(originalRequest);
      } catch (error) {
        // Handle the error
      }
    }
    return Promise.reject(error);
  }
);
export default apiClient;