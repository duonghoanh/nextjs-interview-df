const routes = require('next-routes')();

routes
  .add('home', '/', 'index')
  .add('dashboard', '/dashboard', 'dashboard')
  .add('signin', '/signin', 'signin')
  .add('error', '/error', '_error');

module.exports = routes;