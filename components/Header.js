import { useContext } from 'react';
import { DarkModeContext } from './DarkModeProvider';

const Header = () => {
  const { isDarkMode, setIsDarkMode } = useContext(DarkModeContext);

  return (
    <header className="bg-gray-900 dark:bg-gray-700 text-white py-4 px-6">
      <div className="flex justify-between items-center">
        <h1 className="text-2xl font-bold">Dashboard</h1>
        <div className="flex items-center">
          <button
            className="bg-gray-700 hover:bg-gray-600 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            onClick={() => setIsDarkMode(!isDarkMode)}
          >
            {isDarkMode ? 'Light Mode' : 'Dark Mode'}
          </button>
        </div>
      </div>
    </header>
  );
};

export default Header;