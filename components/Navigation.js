import Link from 'next/link';

const Navigation = () => {
  return (
    <nav className="bg-gray-800 dark:bg-gray-600 text-white py-4 px-6">
      <ul className="flex space-x-4">
        <li>
          <Link href="/dashboard">Dashboard</Link>
        </li>
        <li>
          <Link href="/products">Products</Link>
        </li>
        <li>
          <Link href="/orders">Orders</Link>
        </li>
        <li>
          <Link href="/settings">Settings</Link>
        </li>
      </ul>
    </nav>
  );
};

export default Navigation;