// tests/DarkModeProvider.test.js
import React from 'react';
import { render, screen } from '@testing-library/react';
import { DarkModeProvider, DarkModeContext } from '../components/DarkModeProvider';

test('toggles dark mode correctly', () => {
  const TestComponent = () => {
    const { isDarkMode, setIsDarkMode } = React.useContext(DarkModeContext);
    return (
      <div>
        <button onClick={() => setIsDarkMode(!isDarkMode)}>Toggle Mode</button>
        <p>{isDarkMode ? 'Dark mode' : 'Light mode'}</p>
      </div>
    );
  };

  render(
    <DarkModeProvider>
      <TestComponent />
    </DarkModeProvider>
  );

  expect(screen.getByText('Light mode')).toBeInTheDocument();
  screen.getByRole('button', { name: 'Toggle Mode' }).click();
  expect(screen.getByText('Dark mode')).toBeInTheDocument();
});