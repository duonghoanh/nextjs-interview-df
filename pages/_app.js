import { DarkModeProvider } from '../components/DarkModeProvider';
import { useRouter } from 'next/router';
import '../styles/globals.css';


function MyApp({ Component, pageProps }) {
  const router = useRouter();

  return (
    <DarkModeProvider>
      <Component {...pageProps} />
    </DarkModeProvider>
  );
}

export default MyApp;