import { useState, useEffect, useContext } from 'react';
import { DarkModeContext } from '../components/DarkModeProvider';
import apiClient from '../utils/apiClient';
import Header from '../components/Header';
import Navigation from '../components/Navigation';
import ProjectTable from '../components/ProjectTable';

const Dashboard = () => {
  const { isDarkMode } = useContext(DarkModeContext);
  const [projects, setProjects] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);

  useEffect(() => {
    const fetchProjects = async () => {
      try {
        const response = await apiClient.get('/projects', {
          params: {
            page: currentPage,
            search: searchTerm,
          },
        });
        setProjects(response.data.projects);
        setTotalPages(response.data.totalPages);
      } catch (error) {
        // Handle the error
      }
    };
    fetchProjects();
  }, [currentPage, searchTerm]);

  const handleSearch = (e) => {
    setSearchTerm(e.target.value);
    setCurrentPage(1);
  };

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };
  console.log(projects,'projects');
  return (
    <div
      className={`h-screen ${
        isDarkMode ? 'bg-gray-800 text-white' : 'bg-white text-gray-800'
      }`}
    >
      <Header />
      <Navigation />
      <div className="p-6">
        {/* Search input */}
        <ProjectTable
          projects={projects}
          currentPage={currentPage}
          totalPages={totalPages}
          onPageChange={handlePageChange}
        />
      </div>
    </div>
  );
};

export default Dashboard;