import { useRouter } from "next/router";
import { useEffect } from "react";

const ErrorPage = () => {
  const router = useRouter();

  useEffect(() => {
    // Check for the 'redirect' query parameter
    if (router.query.redirect) {
      // Redirect the user to the specified path
      router.push(router.query.redirect);
    }
  }, [router]);

  return (
    <div className="flex justify-center items-center h-screen">
      <div className="bg-white dark:bg-gray-800 shadow-md rounded px-8 pt-6 pb-8 mb-4 w-full max-w-md">
        {/* button go to login */}
        <button
          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          onClick={() => router.push("/signin")}
        >
          Go to Sign In
        </button>
        {/* error message */}
        <h2 className="text-2xl font-bold mb-4">An error occurred</h2>
        <p>Please try again later.</p>
      </div>
    </div>
  );
};

export default ErrorPage;
