import { useState, useContext } from 'react';
import { DarkModeContext } from '../components/DarkModeProvider';
import apiClient from '../utils/apiClient';
import { useRouter } from 'next/router';

const SignIn = () => {
  const router = useRouter();
  const { isDarkMode } = useContext(DarkModeContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

//   const handleSignIn = async () => {
    // try {
    //   // Call the signin API using the apiClient
    //   const response = await apiClient.post('/signin', { email, password });
    //   // Handle the successful sign-in response
    // } catch (error) {
    //   // Handle the sign-in error
    // }
    // https://frontend-exam.digitalfortress.dev/auth/login
    

//   };

    // https://frontend-exam.digitalfortress.dev/auth/login
    // https://frontend-exam.digitalfortress.dev/auth/refresh-token

    const handleSignIn = async () => {
        try {
            const response = await apiClient.post('/auth/login', { email, password });
            //  Handle the successful sign-in response
            localStorage.setItem('accessToken', response.accessToken)
      localStorage.setItem('refreshToken', response.refreshToken)
            router.push('/dashboard');

            console.log(response);
        } catch (error) {
            console.error(error);
        }
    }

    

  return (
    <div
      className={`flex justify-center items-center h-screen ${
        isDarkMode ? 'bg-gray-800 text-white' : 'bg-white text-gray-800'
      }`}
    >
      <div className="bg-white dark:bg-gray-800 shadow-md rounded px-8 pt-6 pb-8 mb-4 w-full max-w-md">
        <h2 className="text-2xl font-bold mb-4">Sign In</h2>
        <div className="mb-4">
          <label
            className="block text-gray-700 dark:text-gray-300 font-bold mb-2"
            htmlFor="email"
          >
            Email
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 dark:text-gray-300 leading-tight focus:outline-none focus:shadow-outline"
            id="email"
            type="email"
            placeholder="Email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>
        <div className="mb-6">
          <label
            className="block text-gray-700 dark:text-gray-300 font-bold mb-2"
            htmlFor="password"
          >
            Password
          </label>
          <input
            className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 dark:text-gray-300 leading-tight focus:outline-none focus:shadow-outline"
            id="password"
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="flex items-center justify-between">
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            type="button"
            onClick={handleSignIn}
          >
            Sign In
          </button>
        </div>
      </div>
    </div>
  );
};

export default SignIn;